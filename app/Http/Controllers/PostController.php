<?php

namespace App\Http\Controllers;

use App\Post;
use App\CronJob;
use Illuminate\Http\Request;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index( Request $request )
    {
        if ( $request->ajax() ) {
            $cronStatus = CronJob::select('active')
                    ->where(['code' => 'generate_posts'])
                    ->first();

            return [
                'active' => ($cronStatus->active ? $cronStatus->active : 0),
                'posts' => Post::orderBy('updated_at', 'desc')->paginate(15)
            ];
        }
    
        return view('pages.posts.list');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // $post = new Post;

        // $post->content = str_random(150);
        // $post->created_at = date('Y-m-d H:i:s');
        // $post->updated_at = date('Y-m-d H:i:s');
        // $post->save(); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        //
    }

    /**
     * Start post generation
     */
    public function generatePosts() 
    {
        CronJob::where('code', 'generate_posts')
            ->update(['active' => 1]);
        return response()->json(['valid' => true], 200);
    }

    /**
     * Stop post generation
     */
    public function stopGeneratePosts() 
    {
        CronJob::where('code', 'generate_posts')
            ->update(['active' => 0]);
            
        return response()->json(['valid' => true], 200);
    }

    /**
     * Get post list and notice if generation is active
     */
    public function getList ()
    {
        $posts = Post::orderBy('updated_at', 'desc')
                ->paginate(15);
        
        $cronStatus = CronJob::select('active')
                    ->where(['code' => 'generate_posts'])
                    ->first();

        return response()->json([
            'valid' => true, 'list' => $posts, 'jobStatus' => $cronStatus->active
        ]);
    }

}
