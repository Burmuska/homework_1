<?php

namespace App\Console;

use App\CronJob;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\PostGeneration::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // Generate posts
        $schedule->command('post:generate')
            ->cron('*/3 * * * *')
            // ->everyMinute()
            ->when(function () {
                $job = CronJob::select('active')
                    ->where(['code' => 'generate_posts'])
                    ->first();

                if ( $job->active === 1 )
                    return true;
                else
                    return false;
            })
            ->withoutOverlapping();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
