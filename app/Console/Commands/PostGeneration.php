<?php

namespace App\Console\Commands;

use App\Post;
use Illuminate\Console\Command;

class PostGeneration extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'post:generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate post record in db';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $post = new Post;

        $post->content = str_random(100);
        $post->created_at = date('Y-m-d H:i:s');
        $post->updated_at = date('Y-m-d H:i:s');
        $post->save();
    }
}
