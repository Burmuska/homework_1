<meta charset="utf-8">
{{-- <meta name="viewport" content="width=device-width, initial-scale=1"> --}}
<meta name="csrf-token" content="{{ csrf_token() }}">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<title>Apply test - @yield('title')</title>

<link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css" />

<script src="{{ asset('js/app.js') }}" defer></script>