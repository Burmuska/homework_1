<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        @include('layouts.parts.head')
    </head>

    <body>

        <div id="app" class="container" v-cloak>

            <header class="row">
                @include('layouts.parts.header')
            </header>
        
            <div id="main">
                @yield('content')
            </div>
        
            <footer class="row">
                @include('layouts.parts.footer')
            </footer>
        
        </div>

    </body>
</html>
