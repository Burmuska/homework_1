<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Carbon\Carbon;

class CreateCronJobTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('cron_jobs')) {
            Schema::create('cron_jobs', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('code', 255)->comment('cron job code');
                $table->tinyInteger('active')->default(0)->comment('1 - job currently working, 2 - stopped job');
                $table->dateTime('start_date')->nullable();
                $table->dateTime('end_date')->nullable();
                $table->timestamps();
            });
        }

        if (Schema::hasTable('cron_jobs')) {
            // Insert default cron job row for post generation
            
            DB::table('cron_jobs')->insert(
                [
                    'code' => 'generate_posts',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ]
            );
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cron_job');
    }
}
